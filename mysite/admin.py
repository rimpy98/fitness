from django.contrib import admin
from mysite.models import customer,order, category, product, brands, cart, mail, ProductImage,review

admin.site.site_header='Fitness Store'

class productAdmin(admin.ModelAdmin):
    list_display = ['id','product_name','create_date']

admin.site.register(customer)
admin.site.register(category)
admin.site.register(product,productAdmin)
admin.site.register(brands)
admin.site.register(cart)
admin.site.register(mail)
admin.site.register(ProductImage)
admin.site.register(review)
admin.site.register(order)
